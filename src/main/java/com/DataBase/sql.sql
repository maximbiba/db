CREATE DATABASE mySql;
USE mySql;

CREATE TABLE mySql.Street (
                                      Id int(11) NOT NULL AUTO_INCREMENT,
                                      Name varchar(20) DEFAULT NULL,
                                      PRIMARY KEY (Id)
);

CREATE TABLE mySql.Persons (
                                     Id int(11) NOT NULL AUTO_INCREMENT,
                                     FirstName varchar(45) NOT NULL,
                                     LastName varchar(45) NOT NULL,
                                     Age int(11) NOT NULL,
                                     Id_Street int(11) DEFAULT NULL,
                                     PRIMARY KEY (Id)
);

ALTER TABLE Persons
    ADD FOREIGN KEY (Id_Street)
        REFERENCES street(Id);

INSERT INTO Persons (Id, FirstName, LastName, Age, Id_Street) VALUES
(1, 'Maxim', 'Biba', 21, 1),
(2, 'Sasha', 'Zems', 20, 2),
(3, 'Svyat', 'Malanov', 40, 3),
(4, 'Dima', 'Avdeev', 33, null),
(5, 'Maria', 'Fenovka', 19, 5),
(6, 'Vlad', 'Nevzglad', 23, 4),
(7, 'Karina', 'Boiko', 53, 2),
(8, 'Artem', 'Biba', 12, null),
(9, 'Igor', 'Mezencev', 15, 2);
INSERT INTO street (Id, Name) VALUES
                                                                  (1, 'Vitte'),
                                                                  (2, 'Maelstorm'),
                                                                  (3, 'Semerenko'),
                                                                  (4,'Podedi'),
                                                                  (5,'Erevanskaya');
SELECT COUNT(*) FROM Persons;

SELECT AVG(Age) AS AgeAvg FROM Persons;

SELECT DISTINCT LastName FROM Persons ORDER BY LastName;

SELECT LastName, COUNT(*) FROM Persons GROUP BY LastName;

SELECT LastName FROM Persons WHERE LastName LIKE '_%b%_';

SELECT * FROM Persons WHERE id_street IS NULL;

SELECT * FROM Persons JOIN street ON Persons.id_street = street.id
WHERE street.name LIKE '%Semerenko%' AND Persons.age < 18;

SELECT street.id, street.Name, COUNT(Persons.id) FROM street JOIN Persons ON street.id = Persons.id_street
GROUP BY street.id, street.name;

SELECT * FROM street WHERE LENGTH(name) = 6;

SELECT street.id, street.name FROM street JOIN Persons ON street.id = Persons.id_street
GROUP BY street.id, street.name
HAVING COUNT(Persons.id) < 3;